// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMMeshingEngine_CPU.h"
#include "../../DeviceAgnostic/ITMMeshingEngine.h"
#include <iostream>

using namespace ITMLib::Engine;

template<class TVoxel>
ITMMeshingEngine_CPU<TVoxel,ITMVoxelBlockHash>::ITMMeshingEngine_CPU(void) 
{
}

template<class TVoxel>
ITMMeshingEngine_CPU<TVoxel,ITMVoxelBlockHash>::~ITMMeshingEngine_CPU(void) 
{
}

template<class TVoxel>
void ITMMeshingEngine_CPU<TVoxel, ITMVoxelBlockHash>::MeshScene(ITMMesh *mesh, const ITMScene<TVoxel, ITMVoxelBlockHash> *scene)
{
	ITMMesh::Triangle *triangles = mesh->triangles->GetData(MEMORYDEVICE_CPU);

    const TVoxel *localVBA= scene->localVBA.GetVoxelBlocks();
    const ITMHashEntry *hashTable = scene->index.GetEntries();
    const ITMVoxelBlockHash::IndexData *voxelIndex=scene->index.getIndexData();

    typedef ITMVoxelBlockHash TIndex;

	int noTriangles = 0, noMaxTriangles = mesh->noMaxTriangles, noTotalEntries = scene->index.noTotalEntries;
	float factor = scene->sceneParams->voxelSize / (float)SDF_BLOCK_SIZE;

	mesh->triangles->Clear();

	for (int entryId = 0; entryId < noTotalEntries; entryId++)
	{
		Vector3i globalPos;
		const ITMHashEntry &currentHashEntry = hashTable[entryId];

		if (currentHashEntry.ptr < 0) continue;

        globalPos = currentHashEntry.pos.toInt() * SDF_BLOCK_SIZE;


		for (int z = 0; z < SDF_BLOCK_SIZE; z++) for (int y = 0; y < SDF_BLOCK_SIZE; y++) for (int x = 0; x < SDF_BLOCK_SIZE; x++)
		{
			Vector3f vertList[12];

            Vector4f c0,c1,c2;
            int cubeIndex = buildVertList(vertList, globalPos, Vector3i(x, y, z), localVBA, hashTable);

			if (cubeIndex < 0) continue;

			for (int i = 0; triangleTable[cubeIndex][i] != -1; i += 3)
			{
				triangles[noTriangles].p0 = vertList[triangleTable[cubeIndex][i]] * factor;
				triangles[noTriangles].p1 = vertList[triangleTable[cubeIndex][i + 1]] * factor;
				triangles[noTriangles].p2 = vertList[triangleTable[cubeIndex][i + 2]] * factor;

                c0= VoxelColorReader<TVoxel::hasColorInformation, TVoxel, TIndex>::interpolate(localVBA, voxelIndex, vertList[triangleTable[cubeIndex][i]]);
                c1= VoxelColorReader<TVoxel::hasColorInformation, TVoxel, TIndex>::interpolate(localVBA, voxelIndex, vertList[triangleTable[cubeIndex][i+1]]);
                c2= VoxelColorReader<TVoxel::hasColorInformation, TVoxel, TIndex>::interpolate(localVBA, voxelIndex, vertList[triangleTable[cubeIndex][i+2]]);

                if (c0.w > 0.0f) { c0.x /= c0.w; c0.y /= c0.w; c0.z /= c0.w; c0.w = 1.0f; }
                if (c1.w > 0.0f) { c1.x /= c1.w; c1.y /= c1.w; c1.z /= c1.w; c1.w = 1.0f; }
                if (c2.w > 0.0f) { c2.x /= c2.w; c2.y /= c2.w; c2.z /= c2.w; c2.w = 1.0f; }

                triangles[noTriangles].c0.r=static_cast<unsigned char>(c0.x*255);
                triangles[noTriangles].c0.g=static_cast<unsigned char>(c0.y*255);
                triangles[noTriangles].c0.b=static_cast<unsigned char>(c0.z*255);

                triangles[noTriangles].c1.r=static_cast<unsigned char>(c1.x*255);
                triangles[noTriangles].c1.g=static_cast<unsigned char>(c1.y*255);
                triangles[noTriangles].c1.b=static_cast<unsigned char>(c1.z*255);

                triangles[noTriangles].c2.r=static_cast<unsigned char>(c2.x*255);
                triangles[noTriangles].c2.g=static_cast<unsigned char>(c2.y*255);
                triangles[noTriangles].c2.b=static_cast<unsigned char>(c2.z*255);


                if (noTriangles < noMaxTriangles - 1) noTriangles++;
			}


		}
	}

	mesh->noTotalTriangles = noTriangles;
}

template<class TVoxel>
ITMMeshingEngine_CPU<TVoxel,ITMPlainVoxelArray>::ITMMeshingEngine_CPU(void) 
{}

template<class TVoxel>
ITMMeshingEngine_CPU<TVoxel,ITMPlainVoxelArray>::~ITMMeshingEngine_CPU(void) 
{}

template<class TVoxel>
void ITMMeshingEngine_CPU<TVoxel, ITMPlainVoxelArray>::MeshScene(ITMMesh *mesh, const ITMScene<TVoxel, ITMPlainVoxelArray> *scene)
{}

template class ITMLib::Engine::ITMMeshingEngine_CPU<ITMVoxel, ITMVoxelIndex>;
